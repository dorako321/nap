# install python2.7
sudo su -
yum groupinstall -y "Development tools”
yum install -y python-devel

# mod_wsgi
yum install -y mod_wsgi

# repos
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-11.ius.centos6.noarch.rpm

# python27
yum install -y python27 python27-devel python27-setuptools python27-mod_wsgi

# pip
cd /tmp
wget https://raw.github.com/pypa/pip/master/contrib/get-pip.py
python get-pip.py

# fabric
pip install fabric==1.8.1
easy_install 'pycrypto==2.5'
easy_install 'pycrypto==2.5'
exit
fab -V
chmod g-wx,o-wx ~/.python-eggs


# fabric実行
fab -u ec2-user -i /root/nbus.pem -H localhost install