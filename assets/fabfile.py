#-*- coding: utf-8 -*-
import sys, os
from fabric.api import local, sudo, run, put, cd, env
from fabric.contrib.files import exists
from fabric.colors import yellow, green, red
from fabric.utils import abort
# 環境自動構築共通モジュール

# エラーを警告のみに変更
env.warn_only = True

env.postgresqlVersion = '9.3.5';
env.postgisVer = '2.1.4';

# ローカルでの実行コマンド
# fab -u ma-user -i ~/nap.pem -H localhost install

def install():
	# selinuxの無効化
	disabledSelinux();
	# iptablesサービスの停止
	disabledIptables();
	# yumから必要資材をインストール
	installFromYum();
	# Javaのインストール
	installJdk();
	# Play frameworkのインストール
	installPlayframework();
	# Playコンテンツの配置
	deployPlayContents();
	# apacheの初期設定
	initApache();
	# Apacheコンテンツの資材配置
	deployApacheContents();
	# Apacheの開始
	startApache();

	# Xercesのインストール
	installXerces();
	# geosのインストール
	installGeos();
	# PROJのインストール
	installProj();
	# GDAL/OGRをインストール
	installGdalOgr();
	# PostgresSQLのインストール
	installPostgresql();
	# PostGISをインストール
	installPostgis();

	# postgresユーザの作成
	addPostgresUser();
	# DB初期設定
	initPostgresDatabase();
	# Postgres自動起動設定
	setPostgresAutoStart();
	# Postgresサービス内の情報作成
	initPostgresService();
	# ファイルから基本構成をリストア
	restoreDatabase();

# リトライを実行するデコレータ
def retry(tries, delay=1, backoff=2):
	import time
	import math
	if backoff <= 1:
		raise ValueError("backoff must be greater than 1")
	tries = math.floor(tries)
	if tries < 0:
		raise ValueError("tries must be 0 or greater")
	if delay <= 0:
		raise ValueError("delay must be greater than 0")
	def decorator(func):
		def wrapper(*args, **kwargs):
			_tries, _delay = tries, delay
			_tries += 1 #ensure we call func at least once
			while _tries > 0:
				try:
					ret = func(*args, **kwargs)
					return ret
				except Exception as e:
					_tries -= 1
					#retried enough and still fail? raise orignal exception
					if _tries == 0: raise
					time.sleep(_delay)
					#wait longer after each failure
					_delay *= backoff
		return wrapper
	return decorator

# selinuxの無効化
def disabledSelinux():
	print(green("selinuxの無効化"));
	if not exists('/etc/selinux/config.orig', use_sudo=True):
		sudo('cp -p /etc/selinux/config /etc/selinux/config.orig');
	sudo('cp -p /etc/selinux/config.orig /etc/selinux/config');
	sudo('sed -i -e \'s/SELINUX=enforcing/SELINUX=disabled/g\' /etc/selinux/config');

# iptablesサービスの停止
def disabledIptables():
	print(green("iptablesの設定"));
	# 不要なため削除
	sudo('iptables -F');
	sudo('iptables-save');
	sudo('service iptables stop');
	sudo('chkconfig --level 2345 iptables off');

# yumから必要資材をインストール
def installFromYum():
	print(green("yumから必要資材をインストール"));
	sudo("yum update -y");
	sudo("yum install -y httpd");
	sudo("yum install -y git");
	sudo("yum install -y php php-mbstring php-pgsql php-devel php-gd php-pear");
	# 地理情報系
	sudo('yum -y install gd gd-devel');
	sudo('yum -y install giflib giflib-devel');
	sudo('yum -y install libpng libpng-devel');
	sudo('yum -y install libjpeg libjpeg-devel');
	sudo('yum -y install cairo*');
	sudo('yum -y install libxml2 libxml2-devel');
	# ビルドツール
	sudo("yum -y install git screen wget unzip gcc gcc-c++ cmake");
	sudo('yum -y install readline-devel');
	# LIbicuのインストール
	sudo('yum -y install libicu libicu-devel');

# Xercesのインストール
def installXerces():
	with cd('/home/ma-user/nap/assets'):
		if not exists('xerces-c-3.1.2.tar.gz'):
			run('wget http://ftp.riken.jp/net/apache//xerces/c/3/sources/xerces-c-3.1.2.tar.gz');
		run('tar zxvf xerces-c-3.1.2.tar.gz', pty=False);
	with cd('/home/ma-user/nap/assets/xerces-c-3.1.2'):
		run('./configure --prefix=/usr/local/', pty=False);
		run('make', pty=False);
		sudo('make install');
		sudo('/sbin/ldconfig -v');

# geosのインストール
def installGeos():
	with cd('/home/ma-user/nap/assets'):
		if not exists('geos-3.4.2.tar.bz2'):
			run('wget http://download.osgeo.org/geos/geos-3.4.2.tar.bz2');
		run('tar jxvf geos-3.4.2.tar.bz2', pty=False);
	with cd('/home/ma-user/nap/assets/geos-3.4.2'):
		run('./configure --prefix=/usr/local/', pty=False);
		run('make', pty=False);
		sudo('make install');
		sudo('/sbin/ldconfig -v');

# PROJのインストール
def installProj():
	with cd('/home/ma-user/nap/assets'):
		if not exists('proj-4.8.0.tar.gz'):
			run('wget http://download.osgeo.org/proj/proj-4.8.0.tar.gz');
		run('tar zxvf proj-4.8.0.tar.gz', pty=False);
	with cd('/home/ma-user/nap/assets/proj-4.8.0'):
		run('./configure --prefix=/usr/local/', pty=False);
		run('make', pty=False);
		sudo('make install');
		sudo('/sbin/ldconfig -v');

# GDAL/OGRをインストール
def installGdalOgr():
	with cd('/home/ma-user/nap/assets'):
		sudo('yum -y install curl curl-devel unixODBC unixODBC-devel');
		if not exists('gdal-1.9.2.tar.gz'):
			run('wget http://download.osgeo.org/gdal/gdal-1.9.2.tar.gz');
		run('tar zxvf gdal-1.9.2.tar.gz', pty=False);
	with cd('/home/ma-user/nap/assets/gdal-1.9.2'):
		run('./configure' + \
			' --with-libtiff=internal' + \
			' --with-pg' + \
			' --with-pymoddir=/usr/lib/python2.6/site-packages' + \
			' --with-python' + \
			' --with-xerces=/usr/local/' + \
			' --with-odbc' + \
			' --with-geos=/usr/local/bin/geos-config', pty=False);
		run('make', pty=False);
		sudo('make install');

# PostgresSQLのインストール
# Postgresqlのインストール
def installPostgresql():
	print(green("Postgresqlのインストール"));
	with cd('/home/ma-user/nap/assets/'):
		if not exists('postgresql-%s.tar.gz' % env.postgresqlVersion):
			run('wget http://www.ring.gr.jp/pub/misc/db/postgresql/source/v9.3.5/postgresql-%s.tar.gz' % env.postgresqlVersion);
		run('tar zxf postgresql-%s.tar.gz' % env.postgresqlVersion, pty=False);
	# postgresqlのビルドとインストール
	with cd('/home/ma-user/nap/assets/postgresql-%s' % env.postgresqlVersion):
		run('./configure', pty=False);
		run('make', pty=False);
		run('make check', pty=False);
		run('sudo make install');


# PostGISをインストール
def installPostgis():
	initProfile();
	with cd('/home/ma-user/nap/assets'):
		if not exists('postgis-%s.tar.gz' % env.postgisVer):
			run('wget http://download.osgeo.org/postgis/source/postgis-%s.tar.gz' % env.postgisVer);
		run('tar zxvf postgis-%s.tar.gz' % env.postgisVer, pty=False);
	with cd('/home/ma-user/nap/assets/postgis-%s' % env.postgisVer):
		run('./configure' + \
			' --with-gdalconfig='
			' --with-geosconfig=/usr/local/bin/geos-config' + \
			' --with-pgconfig=/usr/local/pgsql/bin/pg_config' + \
			' --with-projdir=/usr/local' + \
			#' --with-proj-libdir=/usr/local/lib'
			'', pty=False);
		run('make', pty=False);
		sudo('make install');

# postgresユーザの作成
def addPostgresUser():
	print(green("postgresユーザの作成"));
	sudo('useradd postgres');

# 環境変数の追加
def setEnvironments():
	print(green("環境変数の追加"));
	if not exists('/home/postgres/.bashrc.orig', use_sudo=True):
		sudo('cp -p /home/postgres/.bashrc /home/postgres/.bashrc.orig');
	sudo('cp -p /home/postgres/.bashrc.orig /home/postgres/.bashrc');
	sudo('echo \'export PGHOME=/usr/local/pgsql/9.3\' | sudo tee -a /home/postgres/.bashrc');
	sudo('echo \'export PGDATA=/usr/local/pgsql/9.3/data\' | sudo tee -a /home/postgres/.bashrc');
	sudo('echo \'export PGHOST=localhost\' | sudo tee -a /home/postgres/.bashrc');
	sudo('echo \'PATH=$PGHOME/bin:$PATH\' | sudo tee -a /home/postgres/.bashrc');

# DB初期設定
def initPostgresDatabase():
	print(green("DB初期設定"));
	sudo('mkdir -p /usr/local/pgsql/9.3/data');
	sudo('chown postgres:postgres /usr/local/pgsql/9.3/data');
	sudo("su - postgres -c 'initdb --encoding=UNICODE'");

# DBファイル設定
def initPostgresConfigFile():
	print(green("DBファイル設定"));
	#postgresql.confの設定追加
	with cd('/usr/local/pgsql/9.3/data'):
		if not exists('postgresql.conf.orig', use_sudo=True):
			sudo('cp -p postgresql.conf postgresql.conf.orig');
		sudo('cp -p postgresql.conf.orig postgresql.conf');
		sudo("echo \"listen_addresses = '*'\" | sudo tee -a postgresql.conf");

		#pg_hba.confの設定追加
		if not exists('pg_hba.conf.orig', use_sudo=True):
			sudo('cp -p pg_hba.conf pg_hba.conf.orig');
		sudo('cp -p pg_hba.conf.orig pg_hba.conf');
		sudo("echo \"host all all 0.0.0.0/0 trust\" | sudo tee -a pg_hba.conf");

# DB動作チェック
def checkPostgres():
	print(green("DB動作チェック"));
	sudo("su - postgres -c 'pg_ctl start'");
	sudo("su - postgres -c 'psql -l'");
	sudo("su - postgres -c 'pg_ctl stop'");

# 起動スクリプト設定
def setPostgresStartScript():
	print(green("postgresql起動スクリプト設定"));
	with cd('/home/ma-user/nap/assets/postgresql-%s/' % env.postgresqlVersion):
		sudo('sudo cp contrib/start-scripts/linux /etc/rc.d/init.d/postgres');
		sudo('chmod 755 /etc/rc.d/init.d/postgres');
		sudo('/etc/rc.d/init.d/postgres start');

# Postgres自動起動設定
def setPostgresAutoStart():
	print(green("Postgres自動起動設定"));
	sudo('chkconfig postgres on');

# Postgresサービス内の情報作成
@retry(6, delay=10)
def initPostgresService():
	print(green("Postgresサービス内の情報作成"));
	# postgresユーザがアクセスできるディレクトリに移動
	with cd('/tmp'):
		# DB起動
		sudo("pg_ctl start", user='postgres');
		# アカウント作成
		result = sudo('psql -c "CREATE USER maint WITH PASSWORD \'maint\' CREATEDB"', user='postgres');
		if(result.return_code != 0):
			raise Exception("failed");
		# スーパーユーザ権限を付与
		result = sudo('psql -c "ALTER ROLE maint WITH SUPERUSER;"', user='postgres');
		if(result.return_code != 0):
			raise Exception("failed");
		# DBデータベース作成
		result = sudo('psql -c "CREATE DATABASE db WITH OWNER = maint ENCODING = \'UTF8\' TABLESPACE = pg_default CONNECTION LIMIT = -1"', user='postgres');
		if(result.return_code != 0):
			raise Exception("failed");

		# postgisの設定
		run("echo \"/usr/local/lib\" >> /etc/ld.so.conf.d/geos.conf");
		# postgisの設定
		result = sudo('psql db -f /usr/local/pgsql/share/contrib/postgis-2.1/postgis.sql', user='postgres');
		result = sudo('psql db -f /usr/local/pgsql/share/contrib/postgis-2.1/spatial_ref_sys.sql', user='postgres');

		# postgisが入ったか確認
		result = sudo('psql -c "select * from postgis_version();"', user='postgres');

		

# ファイルから基本構成をリストア
@retry(6, delay=10)
def restoreDatabase():
	print(green("dbに関数の追加"));
	sudo('cp -rf /home/ma-user/nap/assets/postgres/ /home/postgres/');
	sudo('chown -R postgres:postgres /home/postgres/');
	with cd('/home/postgres/postgres/'):
		result = sudo('find ./db/ -type f -exec psql db -f {} \\;', user='postgres');
		if(result.return_code != 0):
			raise Exception("failed");

# プロファイルの設定
def initProfile():
	if not exists('/etc/profile.orig'):
		sudo('cp -p /etc/profile /etc/profile.orig');
	sudo('cp -p /etc/profile.orig /etc/profile');
	run("echo \"export JAVA_HOME=/usr/java/jdk1.8.0_51\" | sudo tee -a /etc/profile");
	run("echo \"export PATH=$PATH:$JAVA_HOME/bin\" | sudo tee -a /etc/profile");
	run("echo \"export PATH=$PATH:/usr/local/play\" | sudo tee -a /etc/profile");
	run("echo \"export PATH=$PATH:/usr/local/pgsql/bin\" | sudo tee -a /etc/profile");
	run("source /etc/profile");
	local("source /etc/profile");

# Javaのインストール
def installJdk():
	print(green("Javaのインストール"));
	if not exists('jdk-8u51-linux-x64.rpm'):
		run('wget --no-check-certificate --no-cookies' + \
			' - --header "Cookie: oraclelicense=accept-securebackup-cookie"' + \
			' http://download.oracle.com/otn-pub/java/jdk/8u51-b16/jdk-8u51-linux-x64.rpm');
	sudo("rpm -ivh jdk-8u51-linux-x64.rpm");
	initProfile();
	run("java -version");
	
# Play frameworkのインストール
def installPlayframework():
	print(green("Play frameworkのインストール"));
	with cd('/home/ma-user/nap/assets/'):
		run("unzip -o typesafe-activator-1.3.5-minimal.zip");
	if exists('/usr/local/play', use_sudo=True):
		sudo('rm -rf /usr/local/play');
	sudo("mkdir -p /usr/local/play");
	sudo("chown ma-user:ma-user /usr/local/play");
	with cd('/home/ma-user/nap/assets/'):
		run("cp -r activator-1.3.5-minimal/* /usr/local/play/");
	initProfile();
	
# Playコンテンツの配置
def deployPlayContents():
	print(green("Playコンテンツの配置"));
	if exists('/opt/play', use_sudo=True):
		sudo('rm -rf /opt/play');
	sudo("mkdir -p /opt/play");
	sudo("chown ma-user:ma-user /opt/play/");
	run("cp -rp /home/ma-user/nap/src/nap/ /opt/play/nap");
	# 本番環境用に設定シート入れ替え
	with cd('/opt/play/nap/conf/'):
		run("cp -fp application.conf.biz application.conf");
	
# apacheの初期設定
def initApache():
	print(green("apacheの初期設定"));
	# オリジナルをバックアップ作成
	if not exists('/etc/httpd/conf/httpd.conf.orig', use_sudo=True):
		sudo('\cp -f /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.orig');
	# httpd.confの設定
	sudo('\cp -f /home/ma-user/nap/assets/conf/httpd.conf /etc/httpd/conf/httpd.conf');
	
# Apacheコンテンツの資材配置
def deployApacheContents():
	print(green("Apacheコンテンツの資材配置"));
	sudo("chown -R ma-user:ma-user /var/www");
	run("cp -rp /home/ma-user/nap/assets/www/* /var/www/html/");
	
# Apacheの開始
def startApache():
	sudo("/etc/init.d/httpd restart");
