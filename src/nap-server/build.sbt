name := """nap-server"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "org.hibernate" % "hibernate-entitymanager" % "4.2.2.Final",
  "com.google.inject" % "guice" % "3.0",
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc41",
  "org.json"%"org.json"%"chargebee-1.0",
  javaCore,
  javaJdbc,
  javaJpa,
  cache,
  javaWs,
  filters
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
// routesGenerator := InjectedRoutesGenerator

PlayKeys.externalizeResources := false