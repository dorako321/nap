package controllers.api;

import java.util.List;

import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


abstract public class Test extends Controller {
	@Transactional
	public static Result index() {
		try {
			ObjectNode json = Json.newObject();
			ArrayNode array = json.arrayNode();
			array.add("test");
			return ok(array);
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return badRequest();
	}
}
