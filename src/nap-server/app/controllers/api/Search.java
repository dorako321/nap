package controllers.api;

import java.util.List;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import dao.entities.Spot;
import dao.entities.Terminal;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;

/**
 * Javascriptから実行するメソッド
 * 
 * @author gomess
 *
 */
abstract public class Search extends Controller {
	@Transactional
	public static Result index(String q) {
		response().setHeader("Access-Control-Allow-Origin", "*");
		List<Spot> spots;
		try {
			spots = Spot.GetSpotsFromName(q, 10);
			ObjectNode jsona = Json.newObject();
			ArrayNode array = jsona.arrayNode();
			for (Spot spot : spots) {
				ObjectNode json = Json.newObject();
				json.put("spotId", spot.getSpotId());
				json.put("name", spot.getName());
				json.put("ruby", spot.getRuby());
				json.put("thumbnail", spot.getThumbnail());
				json.put("description", spot.getDescription());
				array.add(json);
			}
			return ok(array);
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return badRequest();
	}

	@Transactional
	public static Result detail(Long spotId) {
		response().setHeader("Access-Control-Allow-Origin", "*");
		Spot spot = null;
		Terminal terminal = null;
		Point point = null;

		try {
			ObjectNode json = Json.newObject();
			// 目的地検索
			spot = Spot.findById(spotId);
			// 目的地の座標取得
			Geometry geometry = spot.getGeometry();
			ArrayNode jsonGeo = json.arrayNode();
			if (geometry instanceof Point) {
				point = (Point) geometry;
				jsonGeo.add(point.getY());
				jsonGeo.add(point.getX());
			}
			// 最寄り駅検索
			terminal = Terminal.get(point.getX(), point.getY());

			json.put("spotId", spot.getSpotId());
			json.put("name", spot.getName());
			json.put("ruby", spot.getRuby());
			json.put("thumbnail", spot.getThumbnail());
			json.put("description", spot.getDescription());
			json.put("content", spot.getContent());
			json.put("geometry", jsonGeo);

			// 最寄駅情報追加
			if (terminal != null) {
				json.put("nearestTerminal", terminal.toJson());
			}

			return ok(json);
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return badRequest();
	}
}
