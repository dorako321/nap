package controllers.api;

import java.util.List;

import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import dao.entities.Spot;
import dao.entities.Terminal;
import dao.entities.TerminalType;


abstract public class Timeline extends Controller {
	@SuppressWarnings("deprecation")
	@Transactional
	public static Result get(Double lat, Double lon, Long spotId) {
		response().setHeader("Access-Control-Allow-Origin", "*");
		ObjectNode json = Json.newObject();
		ArrayNode array = json.arrayNode();
		Spot spot = null;
		Point point = null;
		try {
			// 目的地検索
			spot = Spot.findById(spotId);
			// 目的地の座標取得
			Geometry geometry = spot.getGeometry();
			ArrayNode jsonGeo = json.arrayNode();
			if (geometry instanceof Point) {
				point = (Point) geometry;
				jsonGeo.add(point.getY());
				jsonGeo.add(point.getX());
			}

			json.put("spotId", spot.getSpotId());
			json.put("name", spot.getName());
			json.put("ruby", spot.getRuby());
			json.put("thumbnail", spot.getThumbnail());
			json.put("description", spot.getDescription());
			json.put("content", spot.getContent());
			json.put("geometry", jsonGeo);
			
			// 目的地を検索
			Spot spotTo = Spot.findById(spotId);
			geometry = spotTo.getGeometry();
			point = (Point) geometry;		
			
			// 現在地から一番近い停留所を検索
			Terminal terminalFm = Terminal.get(lat, lon);
			if(terminalFm == null){
				json.put("error", "近くに停留所がありません。");
				// 現在地
				array.add(toCurrentLocation(lon, lon));
				// 目的地
				array.add(toJsonNode(spotTo));
				json.put("timeline", array);
				return ok(json);
			}
			

			
			Terminal terminalTo = Terminal.get(point.getX(), point.getY());
			if(terminalTo == null){
				json.put("error", "目的地の近くに停留所がありません。") ;
				// 現在地
				array.add(toCurrentLocation(lon, lon));
				// 目的地
				array.add(toJsonNode(spotTo));
				json.put("timeline", array);
				return ok(json);
			}
			
			// 同じ停留所の場合徒歩
			if(terminalFm.getTerminalId() == terminalTo.getTerminalId()){
				// 現在地
				array.add(toCurrentLocation(lon, lon));
				// 目的地
				array.add(toJsonNode(spotTo));
				json.put("timeline", array);
				return ok(json);
			}
			
			//現在地
			array.add(toCurrentLocation(lat, lon));
			// 乗車停留所名
			array.add(toJsonNode(terminalFm));
			// 異なる停留所の場合同じ系統を持つかを確認
			if(!hasSameTerminalType(terminalFm.getTerminalId(), terminalTo.getTerminalId())){
				// 持たない場合は乗り換えが必須のため築町を追加
				Terminal terminal = Terminal.findById(Long.valueOf(1024));
				array.add(toJsonNode(terminal));
			};
			// 降車停留所
			array.add(toJsonNode(terminalTo));
			// 目的地
			array.add(toJsonNode(spotTo));
			
			json.put("timeline", array);
			
			return ok(json);
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return badRequest();
	}

	private static boolean hasSameTerminalType(Long terminalIdFm, Long terminalIdTo) {
		List<TerminalType> terminalTypeFmList = TerminalType.getByType(terminalIdFm);
		List<TerminalType> terminalTypeToList = TerminalType.getByType(terminalIdTo);
		
		for(TerminalType terminalTypeFm : terminalTypeFmList){
			for(TerminalType terminalTypeTo : terminalTypeToList){
				if(terminalTypeFm.getType() == terminalTypeTo.getType()) return true;
			}
		}
		// 一致するものがない場合
		return false;
	}
	
	private static ObjectNode toCurrentLocation(Double lat, Double lon) {
		ObjectNode node = Json.newObject();
		node.put("type", "currentLocation");
		node.put("name", "現在地");
		node.put("ruby", "げんざいち");
		ObjectNode json = Json.newObject();
		ArrayNode jsonGeo = json.arrayNode();
		jsonGeo.add(lon);
		jsonGeo.add(lat);
		node.put("geometry", jsonGeo);
		return node;
	}

	private static ObjectNode toJsonNode(Terminal terminal) {
		ObjectNode node = Json.newObject();
		node.put("type", "terminal");
		node.put("terminalId", terminal.getTerminalId());
		node.put("name", terminal.getName());
		node.put("ruby", terminal.getRuby());
		
		ObjectNode json = Json.newObject();
		Geometry geometry = terminal.getGeometry();
		ArrayNode jsonGeo = json.arrayNode();
		if (geometry instanceof Point) {
			Point point = (Point) geometry;
			jsonGeo.add(point.getY());
			jsonGeo.add(point.getX());
		}
		node.put("geometry", jsonGeo);
		
		return node;
	}
	
	private static ObjectNode toJsonNode(Spot spot) {
		ObjectNode node = Json.newObject();
		node.put("type", "spot");
		node.put("spotId", spot.getSpotId());
		node.put("name", spot.getName());
		node.put("ruby", spot.getRuby());
		
		ObjectNode json = Json.newObject();
		Geometry geometry = spot.getGeometry();
		ArrayNode jsonGeo = json.arrayNode();
		if (geometry instanceof Point) {
			Point point = (Point) geometry;
			jsonGeo.add(point.getY());
			jsonGeo.add(point.getX());
		}
		node.put("geometry", jsonGeo);
		
		return node;
	}
}
