package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import play.Logger;
import play.db.jpa.JPA;
import utils.Util;
import dao.entities.Spot;

public class SpotDao {
	


	/** 停留所名が存在するかを検索する
	 * @param name 停留所名
	 * @return boolean 存在するなら1、存在しないなら0を返す
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	static public boolean IsBusstopName(String name) throws Exception{
		try{
			if(Util.isBlank(name)) return false;
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT n FROM Spot n WHERE n.name = :name;")
					.setParameter("name", name);
			List<Spot> spots = (List<Spot>) q.getResultList();
			if(spots.isEmpty()) return false;
			return true;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * スポットIDが存在するかを検索する
	 * @param id スポットID
	 * @return boolean 存在するなら1、存在しないなら0を返す
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	static public boolean IsBusstopId(Long spotId) throws Exception{
		try{
			if(Util.IsNull(spotId)) return false;
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT n FROM Spot n WHERE n.spot_id = :id;")
					.setParameter("id", spotId);
			List<Spot> spots = (List<Spot>) q.getResultList();
			if(spots.isEmpty()) return false;
			return true;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	
	/**
	 * 名前からスポット名を取得する
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Spot GetSpotByName(String name) throws Exception {
		try{
			if(Util.isBlank(name)) return null;
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT s FROM Spot s WHERE s.name = :name")
					.setParameter("name", name);
			Spot spots = (Spot) q.getSingleResult();
			return spots;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * IDからスポット名を取得する
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Spot GetSpotById(Long id) throws Exception {
		try{
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT s FROM Spot s WHERE s.id = :id")
					.setParameter("id", id);
			Spot spot = (Spot) q.getSingleResult();
			return spot;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}
	
	/**
	 * 名前からスポット名を取得する
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> GetSpotsByName(String name) throws Exception {
		try{
			if(Util.isBlank(name)) return null;
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT s FROM Spot s WHERE s.name like :name")
					.setParameter("name", name);
			List<Spot> spots = (List<Spot>) q.getResultList();
			if(spots.isEmpty()) return null;
			return spots;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * 名前から似たスポット名を取得する
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> GetSimilarSpotsByName(String name) throws Exception {
		try{
			if(Util.isBlank(name)) return null;
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT s FROM Spot s WHERE s.name like :sname AND s.name NOT LIKE :name")
					.setParameter("sname", '%' + name + '%')
					.setParameter("name", name);
			List<Spot> spots = (List<Spot>) q.getResultList();
			if(spots.isEmpty()) return null;
			return spots;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * 全ての停留所情報を取得する
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> GetAllSpots() throws Exception {
		try{
			EntityManager em = JPA.em();
			Query q = em.createQuery("SELECT s FROM Spot s");
			List<Spot> spots = (List<Spot>) q.getResultList();
			if(spots.isEmpty()) return null;
			return spots;
		}catch(Exception e){
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}



}
