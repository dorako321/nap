package dao.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.hibernate.metamodel.domain.NonEntity;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.jpa.JPA;
import play.libs.Json;
import utils.Util;

@Entity
@Table(name = "terminal_type")
public class TerminalType {

	@Id
	@Column(name = "terminal_type_id")
	private Long terminalTypeId;

	@Required
	@Column(name = "terminal_id")
	private Long terminalId;

	@Required
	@Column(name = "type")
	private Long type;
	
	

	public Long getTerminalTypeId() {
		return terminalTypeId;
	}

	public void setTerminalTypeId(Long terminalTypeId) {
		this.terminalTypeId = terminalTypeId;
	}

	public Long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(Long terminalId) {
		this.terminalId = terminalId;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	/**
	 * 全ての停留所情報を返す
	 */
	@SuppressWarnings("unchecked")
	public static List<TerminalType> findAll() {
		Query query = JPA.em().createQuery("SELECT e FROM TerminalType e");
		return (List<TerminalType>) query.getResultList();
	}

	public static TerminalType findById(Long spotId) {
		return JPA.em().find(TerminalType.class, spotId);
	}



	public static TerminalType get(Long terminalId) {
		return TerminalType.findById(terminalId);
	}

	public static List<TerminalType> getByType(Long terminalId) {
		Query query = JPA.em().createQuery("SELECT e FROM TerminalType e where e.terminalId = :q");
		query.setParameter("q", terminalId);
		return (List<TerminalType>) query.getResultList();
	}
	


}
