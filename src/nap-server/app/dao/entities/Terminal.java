package dao.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.hibernate.metamodel.domain.NonEntity;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.jpa.JPA;
import play.libs.Json;
import utils.Util;

@Entity
@Table(name = "terminal")
public class Terminal {

	@Id
	@Column(name = "terminal_id")
	private Long terminalId;

	@Required
	@Column(name = "name")
	public String name;

	@Required
	@Column(name = "ruby")
	private String ruby;

	@Type(type = "org.hibernate.spatial.GeometryType")
	@Column(name = "geometry")
	private Geometry geometry;

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	/**
	 * ユーザの入力内容のチェック
	 * 
	 * @return
	 */
	public String validate() {
		if (!ruby.matches("^[\\u3040-\\u309F]+$")) {
			return "ルビはひらがなを入力してください。";
		}
		return null;
	}

	/**
	 * 全ての停留所情報を返す
	 */
	@SuppressWarnings("unchecked")
	public static List<Terminal> findAll() {
		Query query = JPA.em().createQuery("SELECT e FROM Spot e");
		return (List<Terminal>) query.getResultList();
	}

	public static Terminal findById(Long spotId) {
		return JPA.em().find(Terminal.class, spotId);
	}

	public Long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(Long spotId) {
		this.terminalId = spotId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRuby() {
		return ruby;
	}

	public void setRuby(String ruby) {
		this.ruby = ruby;
	}

	/**
	 * 座標から一番近い停留所を取得
	 * @param lat
	 * @param lon
	 * @return
	 * @throws Exception 
	 */
	public static Terminal get(Double lat, Double lon) throws Exception {
		try {
			EntityManager em = JPA.em();
			Query q = em
					.createNativeQuery(
							"SELECT s.terminal_id, s.name, s.ruby, s.geometry "
									+ "FROM terminal s "
									+ "WHERE ST_Distance_Sphere(geometry, ST_GeomFromText('POINT(' || "
									+ lat.toString()
									+ " || ' ' || "
									+ lon.toString()
									+ " || ')', 4326)) < 5000 "
									+ "ORDER BY ST_Distance_Sphere(s.geometry, ST_GeomFromText('POINT("
									+ lat.toString() + " " + lon.toString()
									+ ")', 4326)) OFFSET 0 LIMIT 1;", Terminal.class);
			Logger.info(q.toString());
			Terminal terminal = (Terminal) q.getSingleResult();
			return terminal;
		} catch (NoResultException e){
			return null;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			throw e;
		}
	}

	public static Terminal get(Long terminalId) {
		return Terminal.findById(terminalId);
	}
	
	/**
	 * Jsonノードに変換して返す
	 * @param terminal
	 * @return
	 */
	public ObjectNode toJson() {
		ObjectNode json = Json.newObject();
		
		Geometry geometry = this.getGeometry();
		ArrayNode jsonGeo = json.arrayNode();
		Point point = (Point) geometry;
		jsonGeo.add(point.getY());
		jsonGeo.add(point.getX());
		
		json.put("terminalId", this.getTerminalId());
		json.put("name", this.getName());
		json.put("ruby", this.getRuby());
		json.put("geometry", jsonGeo);
		
		return json;
	}

}
