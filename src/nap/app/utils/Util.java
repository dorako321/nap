package utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import play.libs.Json;
import play.mvc.Http.Request;

public class Util {
	/**
	 * 文字列から曜日IDを取得
	 * TODO: 一次対応。そのうちDBで再実装すること
	 * @param str
	 * @return
	 */
	static public Long getWeekId(String str){
		if(str.equals("土曜")) return (long) 1;
		if(str.equals("日祝")) return (long) 2;
		if(str.equals("年末年始")) return (long) 4;
		if(str.equals("元日")) return (long) 5;
		return (long) 0;
	}
	
	/**
	 * nullまたは空文字を判定
	 * @param str
	 * @return
	 */
	static public boolean isBlank(String str){
		if(str == null || str.isEmpty()) return true;
		return false;
	}
	/**
	 * nullを判定
	 * @param val
	 * @return
	 */
	static public boolean IsNull(Long val){
		if(val == null) return false;
		return true;
	}
	/**
	 * 負数を判定
	 * @param val
	 * @return
	 */
	static public boolean IsNegativeNumber(int val){
		if(val < 0) return true;
		return false;
	}
	
	/**
	 * パース可能か
	 * @param timetable
	 * @return ture  : parseable
	 *         flase : oetherwise
	 */
	public static boolean isParseableJson(String timetable) {
		if(timetable == null) return false;
		if(timetable.isEmpty()) return false;
		if(timetable.equals("null")) return false;
		try{
			Json.parse(timetable);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public static String convertToTime(int time) {
		String hour = String.valueOf((time / 60));
		String min = String.valueOf((time % 60));
		if(min.length() == 1) min = "0" + min;
		return hour + ":" + min;
	}
	
	public static String  getIpAddress(){
	    Request req = play.mvc.Http.Context.current().request();
	    return req.getHeader("x-forwarded-for") ;
	}
	
	/**
	 * リストの重複を削除したリストを返す
	 * @param list
	 * @return
	 */
	public static List<String> getDistinctList(List<String> list){
		Set<String> set = new HashSet<String>();
        set.addAll(list);
        List<String> uniqueList = new ArrayList<String>();
        uniqueList.addAll(set);
        return uniqueList;
	}
	
	/**
	 * 現在の曜日タイプを返す
	 * @return	現在の曜日
	 */
	public static String getDayOfType(){ 
	    Calendar cal = Calendar.getInstance(); 
	    switch (cal.get(Calendar.DAY_OF_WEEK)) {
	        case Calendar.SUNDAY: return "日祝";
	        case Calendar.SATURDAY: return "土曜";
	        default: return "平日";
	    }
	}
}
