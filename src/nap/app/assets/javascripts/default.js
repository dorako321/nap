/*global $:false */
/*global Suggest:false */
/*global console:false */

(function() {

    var Nap = function() {};
    Nap = function() {
        this.route = new Nap.Route();
        this.csm = null;
        this.map = null;
    };
    Nap.Route = function() {
        this.objective = '';
        this.count = 0;
        this.err = 0;
        this.errMsg = '';
    };
    Nap.Route.prototype = {
        errReset: function() {
            this.err = 0;
            this.errMsg = '';
        },
        search: function(that, callBack) {
            // エラーのリセット
            this.err = 0;
            this.errMsg = '';

            //検索処理
            $.ajax({
                url: '/v1/routes',
                dataType: 'json',
                data: 'from=' + encodeURI(this.from) + '&to=' + encodeURI(this.to),
                async: true,
                context: this,
                success: function(route) {
                    // 
                    if (route.results === 'error') {
                        this.err = 2;
                        this.errMsg = route.message;
                        callBack(that);
                        return;
                    }
                    that.route.count = route.length;
                    that.route.pathList = route;
                    callBack(that);
                    return;

                },
                error: function() {
                    //異常系
                    this.err = 2;
                    this.errMsg = 'システムエラーです。時間を置いて再度実行するか、ブラウザをリロードして再度お試しください。';
                    callBack(that);
                    return;
                }
            });
        },
        complete: function(that) {
            if (that.route.err > 0) {
                showAlert(that.route.errMsg);
                return;
            }
            if (that.route.err === 2) {
                return;
            }
            // タイトルの変更
            setWindowTitle(that.route.from);
        },

    };

    var createElementComplexSimple = function(element) {
        return createElementComplex(element, null);
    };

    var createElementComplex = function(element, option) {
        var e = $(document.createElement(element));
        if (!option) return e;
        if (!!option.text) e.text(option.text);
        if (!!option.id) e.attr('id', option.id);
        if (!!option.class) e.attr('class', option.class);
        if (!!option.src) e.attr('src', option.src);
        if (!!option.html) e.html(option.html);
        if (!!option.dataToggle) e.attr('dataToggle', option.dataToggle);
        if (!!option.href) e.attr('href', option.href);
        if (!!option.role) e.attr('role', option.role);

        return e;
    };

    var resize = function() {
        $(".mobile-action-bar").css("width", $(window).width() + "px");
    };

    // モバイル検索ウィンドウ開閉アニメーション中にボタンを押されても
    //　動作を無視するために使用するフラグ
    var searchBoxProcess = false;

    // モバイルの検索ウィンドウを表示
    var openMobileSearchBox = function() {
        // 既に開閉アニメーションが動作中の場合実行しない
        if (searchBoxProcess) return;
        searchBoxProcess = true;
        $(".mobile-action-bar").animate({
            height: "100px",
        }, 500);
        $(".mobile-search-bar").fadeIn(500);
        searchBoxProcess = false;
        $("#intro").animate({
            height: "100px",
        }, {
            duration: 500,
            complete: function() {
                searchBoxProcess = false;
            }
        });
    };
    // モバイルの検索ウィンドウを閉じる
    var closeMobileSearchBox = function() {
        // 既に開閉アニメーションが動作中の場合実行しない
        if (searchBoxProcess) return;
        searchBoxProcess = true;
        $(".mobile-action-bar").animate({
            height: "46px",
        }, 500);
        $(".mobile-search-bar").fadeOut(500);
        searchBoxProcess = false;
        $("#intro").animate({
            height: "46px",
        }, {
            duration: 500,
            complete: function() {
                searchBoxProcess = false;
            }
        });
    };

    // クッキー情報からテキストをセット
    var setInputByCookie = function() {
        if ((!$('#input-objective').val()) && $.cookie('input-objective')) {
            $('#input-objective').val($.cookie('input-objective'));
        }
    };

    // クッキー情報の登録　モバイル用
    var setMobileCookie = function(objective) {
        $.cookie('input-objective', objective, {
            expires: 30
        });
    };

    //エラーメッセージを出力する
    var showAlert = function(str) {
        var alert = $("#alert");
        var div = createElementComplex('div', {
            'class': 'alert alert-warning fade in',
            'text': str
        });
        alert.empty();
        alert.append(div);
        alert.show(250);
    };
    var hideAlert = function() {
        $("#alert").hide(250);
    };

    // 初期画面を閉じる
    var closeExplainPage = function() {
        $(".explain").hide();
    };

    // アクションバーのロゴ表示
    var showActionBarLogo = function() {
        $('#action-bar-logo').show();
    };

    // 検索処理
    var search = function(objective) {
        // 乗車停留所と降車停留所のクッキー情報保存
        // setMobileCookie(objective);
        // モバイルの検索画面を閉じる
        closeMobileSearchBox();
        // 検索
        (function() {
            nap.route.objective = objective;
            nap.route.search(nap, nap.route.complete);
        })();
        // 初期画面を閉じる
        closeExplainPage();
        //　アクションバーのロゴ表示
        showActionBarLogo();
    };

    // バウンスアニメーション
    var bounceAnimation = function(element) {
        setInterval(function() {
            element.toggleClass('bounce animated');
            element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                $(e.target).removeClass('bounce animated');
            });
        }, 4000);

        element.mouseover(function() {
            element.toggleClass('bounce animated');
            element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                $(e.target).removeClass('bounce animated');
            });
        });
    };

    // ウィンドウタイトルの変更
    var setWindowTitle = function(title) {
        document.title = title + ' - 長崎観光情報検索サイト nap!';
    };

    // PCかスマホかの判定
    var isSmartPhone = function() {
        var ua = navigator.userAgent;
        if ((ua.indexOf('iPhone') > 0 && ua.indexOf('iPad') == -1) || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0) {
            return true;
        }
        return false;
    };

    var acceptGeolocationApi = function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                // success callback
                function(position) {
                    alert("現在地情報の利用が許可されました。");
                    // latitude: " + position.coords.latitude + "\nlongitude: " + position.coords.longitude
                },
                // error callback
                function(position) {
                    alert("現在地情報の利用が許可されませんでした。ウェブサービスの一部機能が制限されます。");
                }
            );
        }
    };
    // 生成
    var nap = new Nap();


    $(document).ready(function() {

        // 検索候補出力サジェストのロード
        $("#input-objective").autocomplete({
            source: function(req, resp) {
                $.ajax({
                    url: '/api/v1/suggest',
                    type: 'get',
                    cache: true,
                    dataType: 'json',
                    data: {
                        q: req.term
                    },
                    success: function(o) {
                        resp(o);
                    },
                    error: function(xhr, ts, err) {
                        resp(['']);
                    }
                });

            }
        });

        bounceAnimation($('#square'));

        resize();

        // モバイル検索画面を閉じた場合の処理
        $("#mobile-search-close-button").on('click touchstart', function() {
            //window.alert("click close button");
            closeMobileSearchBox();
        });

        // モバイル検索画面を開いた場合の処理
        $(".action-bar-search-button").on('click touchstart', function() {
            $("#search").tooltip('hide');
            openMobileSearchBox();
        });

        // 検索ボタンの押下
        $(".mobile-btn").click(function() {
            var objective = $('#input-objective').val();
            if (!objective) {
                window.alert('観光地名を入力してください');
                return;
            }
            search(objective);
        });

        // 検索する(イントロ)ボタン
        $('#intro-btn').click(function() {
            closeMobileSearchBox();
            $("html,body").animate({
                scrollTop: 0
            }, "slow");
            $("#search").tooltip('show');
        });

        // 現在地を許可
        $('#accept-geolocation-api').click(function() {
            acceptGeolocationApi();
        });

        // 検索ボタンの表示をPCとモバイルで切り替え
        displaySearchButton();


    });

    $(window).resize(function() {
        resize();
    });




})();
