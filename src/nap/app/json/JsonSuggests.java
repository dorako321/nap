package json;

import java.util.List;


public class JsonSuggests {
	private String name;

	private String ruby;

	private List<JsonSuggestDetails> spots;

	public JsonSuggests(String name, String ruby, List<JsonSuggestDetails> details){
		this.name = name;
		this.ruby = ruby;
		this.spots = details;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRuby() {
		return ruby;
	}

	public void setRuby(String ruby) {
		this.ruby = ruby;
	}

	public List<JsonSuggestDetails> getSpots() {
		return spots;
	}

	public void setSpots(List<JsonSuggestDetails> spots) {
		this.spots = spots;
	}


}
