/**
 *
 */
package json;

import java.util.List;

/**
 * @author gomess
 *
 */
public class JsonRoutes {
	private JsonSpots from;

	private JsonSpots to;

	private Long companyId;

	private String companyName;

	public JsonRoutes(JsonSpots jsonFm, JsonSpots jsonTo, Long companyId,
			String companyName) {
		this.from = jsonFm;
		this.to = jsonTo;
		this.companyId = companyId;
		this.companyName = companyName;
	}
	public JsonSpots getFm() {
		return from;
	}
	public void setFm(JsonSpots fm) {
		this.from = fm;
	}

	public JsonSpots getTo() {
		return to;
	}

	public void setTo(JsonSpots to) {
		this.to = to;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
