package json;

import dao.entities.Spot;

public class JsonSpots {

	private Long spotId;

	private String name;

	private String ruby;

	private String localId;

	private Long spotTypeId;

	private String spotTypeName;
	
	public JsonSpots(Spot spots){
		this.spotId = spots.getSpotId();
		this.name = spots.getName();
		this.ruby = spots.getRuby();
		this.spotTypeId = spots.getSpotId();
	}

	public Long getSpotId() {
		return spotId;
	}

	public void setSpotId(Long spotId) {
		this.spotId = spotId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRuby() {
		return ruby;
	}

	public void setRuby(String ruby) {
		this.ruby = ruby;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}

	public Long getSpotTypeId() {
		return spotTypeId;
	}

	public void setSpotTypeId(Long spotTypeId) {
		this.spotTypeId = spotTypeId;
	}

	public String getSpotTypeName() {
		return spotTypeName;
	}

	public void setSpotTypeName(String spotTypeName) {
		this.spotTypeName = spotTypeName;
	}
}
