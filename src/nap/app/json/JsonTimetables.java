package json;

public class JsonTimetables {
	private String toTime;
	private String fmTime;
	private String destination;
	private String direction;
	private String transitTime;
	private String bill;
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public String getFmTime() {
		return fmTime;
	}
	public void setFmTime(String fmTime) {
		this.fmTime = fmTime;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getTransitTime() {
		return transitTime;
	}
	public void setTransitTime(String transitTime) {
		this.transitTime = transitTime;
	}
	public String getBill() {
		return bill;
	}
	public void setBill(String bill) {
		this.bill = bill;
	}
	

	
}
