package form;

import javax.persistence.Column;

import play.data.validation.Constraints.Required;

public class BusstopForm {
	
	public Long spotId;

	@Required
	public String name;

	@Required
	public String ruby;

}
