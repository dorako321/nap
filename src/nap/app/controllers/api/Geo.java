package controllers.api;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import dao.entities.Spot;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;

/**
 * Javascriptから実行するメソッド
 * 
 * @author gomess
 *
 */
abstract public class Geo extends Controller {
	
	/**
	 * 指定した日付に関する情報を取得する
	 * @param date
	 * @return
	 */
	@Transactional
	public static Result getAboutToday(){
		ObjectNode json = getAboutDay(new Date());
		return ok(json);
	}

	private static ObjectNode getAboutDay(Date date) {
		ObjectNode json = Json.newObject();
		String[] weekName = {"日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"};
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
	    int month = calendar.get(Calendar.MONTH) + 1;
	    int day = calendar.get(Calendar.DATE);
		int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		json.put("ymd", year + "年" + month + "月" + day + "日");
		json.put("weekName", weekName[week]);
		return json;
	}

	protected static ObjectNode buildError(String message){
		ObjectNode json = Json.newObject();
		json.put("results", "error");
		json.put("message", message);
		return json;
	}
	
	/**
	 * 指定した座標付近の位置情報を出力する
	 * @param lat
	 * @param lon
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@Transactional
	public static Result getLocationArea(Double lat, Double lon){
		try {
			ObjectNode json = Json.newObject();
			ArrayNode array = json.arrayNode();
			List<Spot> spots = Spot.getLocationArea(lat, lon, 10);
			for (Spot spot : spots) {
				ObjectNode jsonGeometry = Json.newObject();
				jsonGeometry.put("type", "Point");

				// 位置情報
				Geometry geometry = spot.getGeometry();
				ArrayNode jsonGeo = json.arrayNode();
				if (!(geometry instanceof Point))
					continue;
				Point point = (Point) geometry;
				jsonGeo.add(point.getY());
				jsonGeo.add(point.getX());

				jsonGeometry.put("coordinates", jsonGeo);

				ObjectNode properties = Json.newObject();
				properties.put("title", spot.getName());
				properties.put("description", "test");
				properties.put("marker-color", "#fc4353");
				properties.put("marker-size", "small");

				ObjectNode element = Json.newObject();
				element.put("type", "Feature");
				element.put("geometry", jsonGeometry);
				element.put("properties", properties);
				array.add(element);
			}
			return ok(array);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return badRequest();
	}

}
