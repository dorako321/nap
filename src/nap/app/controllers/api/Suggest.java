package controllers.api;

import java.util.List;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.entities.Spot;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;

/**
 * Javascriptから実行するメソッド
 * 
 * @author gomess
 *
 */
abstract public class Suggest extends Controller {
	@Transactional
	public static Result getAutoComplete(String q) {
		List<Spot> spots;
		try {
			spots = Spot.GetSpotsFromName(q, 10);
			ObjectNode json = Json.newObject();
			ArrayNode array = json.arrayNode();
			for (Spot spot : spots) {
				array.add(spot.getName());
			}
			return ok(array);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return badRequest();
	}
}
