package controllers;

import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import dao.SpotDao;
import dao.entities.Spot;
import play.Play;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;
import utils.Util;
import views.html.*;

public class Application extends Controller {

	/**
	 * 停留所情報入力用フォーム
	 */
	final static Form<Spot> busstopForm = Form.form(Spot.class);

	@Transactional
	public static Result index() {
		return ok(index.render());
	}

	/**
	 * 停留所一覧
	 * 
	 * @return
	 */
	@Transactional
	public static Result busstops() {
		return TODO;
	}

	/**
	 * 近くのスポットをJson形式で出力する
	 * 
	 * @param lat
	 * @param lon
	 * @param num
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@Transactional
	public Result getNeighborSpots(double lat, double lon, int num) {
		ObjectNode json = Json.newObject();
		ArrayNode array = json.arrayNode();
		List<Spot> stops = null;
		try {
			stops = Spot.GetSpotsFromPos(lat, lon, num);
			for (Spot spot : stops) {
				ObjectNode jsonGeometry = Json.newObject();
				jsonGeometry.put("type", "Point");

				// 位置情報
				Geometry geometry = spot.getGeometry();
				ArrayNode jsonGeo = json.arrayNode();
				if (!(geometry instanceof Point))
					continue;
				Point point = (Point) geometry;
				jsonGeo.add(point.getY());
				jsonGeo.add(point.getX());

				jsonGeometry.put("coordinates", jsonGeo);

				ObjectNode properties = Json.newObject();
				properties.put("title", spot.getName());
				properties.put("description", "test");
				properties.put("marker-color", "#fc4353");
				properties.put("marker-size", "small");

				ObjectNode element = Json.newObject();
				element.put("type", "Feature");
				element.put("geometry", jsonGeometry);
				element.put("properties", properties);

				array.add(element);
			}

			return ok(array);
		} catch (Exception e) {
			json.put("results", "error");
			return badRequest(json);
		}
	}


}
