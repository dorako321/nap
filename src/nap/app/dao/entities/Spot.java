package dao.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Geometry;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.jpa.JPA;
import utils.Util;

@Entity
@Table(name = "spots")
public class Spot {

	@Id
	@Column(name = "spot_id")
	private Long spotId;

	@Required
	@Column(name = "name")
	public String name;

	@Required
	@Column(name = "ruby")
	private String ruby;

	@Type(type = "org.hibernate.spatial.GeometryType")
	@Column(name = "geometry")
	private Geometry geometry;

	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	/**
	 * ユーザの入力内容のチェック
	 * 
	 * @return
	 */
	public String validate() {
		if (!ruby.matches("^[\\u3040-\\u309F]+$")) {
			return "ルビはひらがなを入力してください。";
		}
		return null;
	}

	/**
	 * 全ての停留所情報を返す
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> findAll() {
		Query query = JPA.em().createQuery("SELECT e FROM Spot e");
		return (List<Spot>) query.getResultList();
	}

	public static Spot findById(Long spotId) {
		return JPA.em().find(Spot.class, spotId);
	}

	public Long getSpotId() {
		return spotId;
	}

	public void setSpotId(Long spotId) {
		this.spotId = spotId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRuby() {
		return ruby;
	}

	public void setRuby(String ruby) {
		this.ruby = ruby;
	}

	/**
	 * 位置情報から近い停留所名を取得する
	 * 
	 * @param id
	 *            スポットID
	 * @return boolean 存在するなら1、存在しないなら0を返す
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> GetSpotsFromPos(double lat, double lon, int num)
			throws Exception {
		try {
			if (Util.IsNegativeNumber(num))
				return null;
			EntityManager em = JPA.em();
			Query q = em
					.createNativeQuery(
							"SELECT s.spot_id, s.name, s.ruby s.geometry"
									+ "FROM stops s "
									+ "WHERE ST_Distance_Sphere(geometry, ST_GeomFromText('POINT(' || :lat || ' ' || :lon || ')', 4326)) < 1000 "
									+ "ORDER BY ST_Distance_Sphere(g.geometry, ST_GeomFromText('POINT(32.75332372 129.8710989)', 4326)) OFFSET 0 LIMIT (:num);",
							Spot.class);
			q.setParameter("lat", lat);
			q.setParameter("lon", lon);
			q.setParameter("num", num);
			List<Spot> spots = q.getResultList();
			if (spots.isEmpty())
				return null;
			return spots;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * 位置情報から近い停留所名を取得する
	 * 
	 * @param id
	 *            スポットID
	 * @return boolean 存在するなら1、存在しないなら0を返す
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> GetSpotsFromName(String query, int num)
			throws Exception {
		try {
			if (Util.IsNegativeNumber(num))
				return null;
			EntityManager em = JPA.em();
			Query q = em
					.createQuery(
							"SELECT s "
									+ "FROM Spot s "
									+ "WHERE s.name like :query "
									+ "OR s.ruby like :query ",
							Spot.class);
			q.setParameter("query", "%" + query + "%");

			List<Spot> spots = q.getResultList();
			if (spots.isEmpty())
				return null;
			return spots;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

	/**
	 * 指定した座標から近いスポットを出力する
	 * 
	 * @param lat
	 * @param lon
	 * @param num
	 * @return
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static List<Spot> getLocationArea(Double lat, Double lon, Integer num) throws Exception {
		try {
			if (Util.IsNegativeNumber(num))
				return null;
			EntityManager em = JPA.em();
			Query q = em
					.createNativeQuery(
							"SELECT s.spot_id, s.name, s.ruby, s.geometry "
									+ "FROM spots s "
									+ "WHERE ST_Distance_Sphere(geometry, ST_GeomFromText('POINT(' || "
									+ lat.toString()
									+ " || ' ' || "
									+ lon.toString()
									+ " || ')', 4326)) < 5000 "
									+ "ORDER BY ST_Distance_Sphere(s.geometry, ST_GeomFromText('POINT("
									+ lat.toString() + " " + lon.toString()
									+ ")', 4326)) OFFSET 0 LIMIT ("
									+ num.toString() + ");", Spot.class);
			List<Spot> spots = q.getResultList();
			if (spots.isEmpty())
				return null;
			return spots;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			throw new Exception("Exception");
		}
	}

}
