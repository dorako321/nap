name := """nap"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

Concat.parentDir := "public"

pipelineStages := Seq(digest, gzip)

pipelineStages in Assets := Seq(concat, uglify, digest, gzip)

LessKeys.compress := true

LessKeys.compress in Assets := true

includeFilter in (Assets, LessKeys.less) := "*.less"

UglifyKeys.uglifyOps := { js =>
  Seq((js.sortBy(_._2), "default.min.js"))
}


libraryDependencies ++= Seq(
  "org.hibernate" % "hibernate-entitymanager" % "4.2.2.Final",
  "com.google.inject" % "guice" % "3.0",
  "org.postgresql" % "postgresql" % "9.3-1102-jdbc41",
  "org.json"%"org.json"%"chargebee-1.0",
  "net.sourceforge.htmlunit" % "htmlunit" % "2.13",
  "org.seleniumhq.selenium" % "selenium-java" % "2.45.0" % "test",
  javaCore,
  javaJdbc,
  javaJpa,
  cache,
  javaWs,
  filters
)


